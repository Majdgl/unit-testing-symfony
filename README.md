# Les différents types de tests

    + Tests unitaires
    + Tests fonctionnels
    + Tests End to End

# Comment tester ?

    + Tester pour vérifier
    + Test Driven Development
    + Tests first

# Outils

    + PHPUnit
    + Behat/Puppeteer/Cypress       

# Pour le tests unitaires on va utiliser PHPUnit:

        $ composer require --dev phpunit/phpunit symfony/test-pack

<!--
    Write test cases in the tests/ folder
    Use MakerBundle's make:test command as a shortcut!
    Run the tests with php bin/phpunit     
-->

        <?php

        namespace App\Tests\Service;

        use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

        class AppTest extends KernelTestCase {

            public function testSomething() {
                
                $this->assertEquals(2, 1+1);
            }

        }
        ?>


* Run your test with CLI:

        $ php ./vendor/bin/phpunit

# Tester avec une base de données        
   
   + On va utiliser DoctrineFixturesBundle

            $ composer require --dev orm-fixtures


    + Créer notre entity User

            $ composer require symfony/maker-bundle --dev
            $ composer require security
            $ php bin/console make:user

    + Créer un nouvel class UserFixtures sous namespace App\DataFixtures

            <?php

            namespace App\DataFixtures;

            use Doctrine\Bundle\FixturesBundle\Fixture;
            use Doctrine\Persistence\ObjectManager;

            class UserFixtures extends Fixture {

                public function load(ObjectManager $manager) {
                    
                }
            }
            ?>
